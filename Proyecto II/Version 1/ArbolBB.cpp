#include "ArbolBB.h"

ArbolBB::ArbolBB()
{
	//Constructor del arbol
	Raiz = nullptr;

}

Nodo* ArbolBB::insertarNodo(Nodo* nodo, int numero)
{
	/*
	Metodo para insertar nodos con valores en el arbol mediante 
	recursion, colocando los valores menores a la izquierda a la raiz
	y los mayores a la derecha de la raiz
	*/
	if (nodo == nullptr) {
		Nodo* nuevoNodo = new Nodo(numero);
		return nuevoNodo;
	}
	else if (numero < nodo->Valor) {
		nodo->Izq = insertarNodo(nodo->Izq, numero);
		return nodo;
	}
	else {
		nodo->Der = insertarNodo(nodo->Der, numero);
		return nodo;
	}
}

void ArbolBB::insertar(int numero)
{
	/*
	Metodo para iniciar la recurci�n para insertat el valor 
	en un nodo del arbol seg�n su grado
	*/
	Raiz = insertarNodo(Raiz, numero);
}

bool ArbolBB::buscar(int num)
{
	/*
	Metodo para buscar valores enteros dentro del arbol recorriendolo
	y retornando true si lo encuentra else si no (iteracion)
	*/
	Nodo* nodoActual = Raiz;
	while (nodoActual != nullptr) {
		if (nodoActual->Valor == num)
			return true;
		else if (num < nodoActual->Valor)
			nodoActual = nodoActual->Izq;
		else {
			nodoActual = nodoActual->Der;
		}
	}
	return false;
}

void ArbolBB::buscarArreglo(vector<int> arreglo)
{
	/*
	Metodo recibe un arreglo de enteros y lo procesa para 
	buscar cada uno de sus elementos en el arbol
	*/
	int valor;
	for (int i = 0; i < arreglo.size(); i++) {
		valor = arreglo[i];
		buscar(valor);
	}
}



void ArbolBB::imprimirArbol()
{
	/*
	Metodo para iniciar recursion 
	*/
	toString(Raiz, 0);
}

void ArbolBB::toString(Nodo* nodo, int contador)
{
	/*
	Metodo para mostrar el arbol en consola desde los valores menores a la 
	izquierda hacia la derecha recursivamente
	*/
	if (nodo == nullptr) {
		return;
	}
	else {
		toString(nodo ->Izq, contador + 1);
		for (int i = 0; i < contador; i++) {
			cout << " ";
		}
		cout << nodo->Valor << endl;
		toString(nodo->Der, contador + 1);

	}
}
