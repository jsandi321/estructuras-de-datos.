#pragma once
#include<iostream>
#include"Nodo.h"
#include<vector>
using namespace std;

/*
Clase arbol binario de busqueda beta
*/
class ArbolBB
{
public:
	//Metodos de la clase
	Nodo* Raiz;
public:
	//Metodos de la clase 
	ArbolBB();
	Nodo* insertarNodo(Nodo* nodo,  int numero);
	void insertar(int numero);
	bool buscar(int num);
	void buscarArreglo(vector<int> arreglo);
	void imprimirArbol();
	void toString(Nodo* nodo, int contador);
};

