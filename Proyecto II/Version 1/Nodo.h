//Clase nodo para el arbol binario de busqueda
#pragma once
class Nodo
{
public:
	//Atributos de la clase Nodo
	int Valor;
	Nodo* Izq;
	Nodo* Der;
public:
	//Metodos de la clase Nodo
	Nodo(int valor);
	bool esHoja();
	bool esInterno();
	int grado();
};
