// ArbolBinario.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//Jeison Sandi Mena
/*
Version de pruebas de proyecto II Estructuras de datos con 
implementacion de Arbol Binario de Busqueda y AVL y metodos
para correr las pruebas de tiempo
*/
#include <iostream>
#include<stdlib.h>
#include<time.h>
#include"ArbolBB.h"
#include <vector>
#include<ctime>
#include <list>
#include<algorithm>
using namespace std;

//Plantillas
vector<int> generadorNumeros(int cantidad);
ArbolBB rellenarArbol(vector<int> arreglo, ArbolBB arbol);
bool buscarLL(list<int> lista, int valor);
//Metodo para el experimentos
double experimentoLL(int numero); //Experimento lista enlazada (Linked List)
double experimentoABB(int numero); //Experimento arbol
//Repeticiones
double repetirExperimentoLL(int numero);
double repetirExperimentoABB(int numero);
//void verArbol(ArbolBB arbol, int n);

int main()
{
    //Experimentos fase 1
    double experimento1LL = repetirExperimentoLL(10);
    double experimento1ABB = repetirExperimentoABB(10); //Repite las pruebas para el arbol binario
    cout << "Tiempo Experimento 1 Lista Enlazada: " << experimento1LL << endl;
    cout << "Tiempo Experimento 1 Arbol Binario de Busqueda: " << experimento1ABB << endl;
    //Experimento fase 2
    double experimento2LL = repetirExperimentoLL(1000);
    double experimento2ABB = repetirExperimentoABB(1000); //Repite las pruebas para el arbol binario
    cout << "Tiempo Experimento 2 Lista Enlazada: " << experimento2LL << endl;
    cout << "Tiempo Experimento 2 Arbol Binario de Busqueda: " << experimento2ABB << endl;
    //Experiento fase3
    double experimento3LL = repetirExperimentoLL(100000);
    double experimento3ABB = repetirExperimentoABB(100000); //Repite las pruebas para el arbol binario
    cout << "Tiempo Experimento 3 Lista Enlazada: " << experimento3LL << endl;
    cout << "Tiempo Experimento 3 Arbol Binario de Busqueda: " << experimento3ABB << endl;
}

vector<int> generadorNumeros(int cantidad) {
    /*
    Metodo se encarga de generar una cantidad determinada
    de numeros al azar en un rango especifico y los retorna
    en un arreglo para ser ingresados en las estructuras de 
    los experimentos
    */
    srand(time(NULL));
    int num;
    vector<int> arreglo;
    for (int i = 0; i < cantidad; i++) {
        num = rand() % 1000000;
        //cout << num << " ";
        arreglo.push_back(num);
    }
    return arreglo;
}

ArbolBB rellenarArbol(vector<int> arreglo, ArbolBB arbol)
{
    /*
    Metodo encargado de insertar en el arbol binario los valores
    del arreglo de numeros y retirna el arbol
    */
    int largo = arreglo.size();
    for (int i = 0; i < largo; i++) {
        arbol.insertar(arreglo[i]);
    }
    return arbol;
}

bool buscarLL(list<int> lista, int valor)
{
    for (list<int>::iterator iterador = lista.begin(); iterador != lista.end(); iterador++) {
        if (*iterador == valor)
            return true;
    }
}

double experimentoLL(int numero)
{
    /*
    Metodo para ejecutar el experimento para la lista enlazada
    */
    unsigned t0, t1;
    list<int> lista;
    vector<int> arreglo = generadorNumeros(numero);
    t0 = clock();
    for (int i = 0; i < arreglo.size(); i++) {
        lista.push_back(arreglo[i]);
    }
    lista.sort(); //Ordena la lista
    //Algoritmo para buscar los elementos y retornar el tiempo
    for (int i = 0; i < arreglo.size(); i++) {
        buscarLL(lista, arreglo[i]);
    }
    t1 = clock();
    double time = (double(t1 - t0) / CLOCKS_PER_SEC);
    return time;
}

double experimentoABB(int numero)
{
    /*
    Se encarga de ejecutar los metodos requeridos para el experimento
    del arbol binario y retorna un dounble con el tiempo de ejecucion
    */
    unsigned t0, t1;
    ArbolBB arbolBinario;
    vector<int> arreglo = generadorNumeros(numero);
    arbolBinario = rellenarArbol(arreglo, arbolBinario);
    t0 = clock();
    arbolBinario.buscarArreglo(arreglo);
    t1 = clock();
    double time = (double(t1 - t0) / CLOCKS_PER_SEC);
    return time;
    /*
    Faltantes: Correr la prueba 30 veces y calcular el promedio de cada prueba
    Se toma el tiempo solo de la busqueda de los numeros
    */
}

double repetirExperimentoLL(int numero)
{
    /*
    Metodo para ejecutar las repeticiones del experimento de la lista enlazada
    */
    double tiempoAcumulado = 0;
    double promedio; 
    for (int i = 0; i < 30; i++) {
        double tiempo = experimentoLL(numero);
        tiempoAcumulado += tiempo;
    }
    promedio = tiempoAcumulado / 30;
    return promedio;
}

double repetirExperimentoABB(int numero)
{
    /*
    Metodo para la repeticion del experimento, retorna el 
    tiempo que tardo en realizar las busquedas
    */
    double tiempoAcumulado = 0;
    double promedio;
    for (int i = 0; i < 30; i++) {
        double tiempo = experimentoABB(numero); //Obtiene el tiempo de una corrida
        tiempoAcumulado += tiempo;  //La suma para obtener un acumulado de las pruebas
    }
    promedio = tiempoAcumulado / 30;    //Calcular promedio del tiempo de ejecucion
    return promedio;
}

/*
Metodo de prueba para mostrar arbol (borrar)
void verArbol(ArbolBB arbol, int n)
{
    if (arbol.Raiz == nullptr) {
        return;
    }
    verArbol(arbol->, n + 1);

    for (int i = 0; i < n; i++)
        cout << "   ";

    numNodos++;
    cout << arbol->nro << endl;

    verArbol(arbol->izq, n + 1);
}
*/